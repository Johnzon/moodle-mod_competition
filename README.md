# Moodle Competition Plugin

A Kaggle-like plugin for Moodle that allows users to enter data classification competitions. This is the plugin used for [biometric-competitions.com](biometric-competitions.com).